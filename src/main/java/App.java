import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

public class App {

    public static void main(String[] args) throws IOException, ParseException {

        HttpURLConnection connection = getConnection("http://localhost:8080/products");
        String readLine = "";
        StringBuffer response = readResponse(connection, readLine);
        int responseCode = connection.getResponseCode();
        String productDescription = "";
        String seller = "";
        int productId = 0;

        //initial read of all products
            if (responseCode == HttpURLConnection.HTTP_OK) {
                Object obj= new JSONParser().parse(response.toString());
                JSONArray ja = (JSONArray) obj;
                Iterator itr = ja.iterator();

                System.out.println("\nPrzedmioty z hardcodowanej listy produktów : "
                                + connection.getURL()
                                + "\n");

                for(int i = 0; ja.size() > i; i++) {

                    JSONObject jo = (JSONObject) itr.next();
                    productId = Integer.parseInt(jo.get("id").toString());
                    productDescription = jo.get("description").toString();

                    if(jo.get("seller") != null){
                        seller = jo.get("seller").toString();
                    }else{
                        seller = "null";
                    }

                    System.out.println("ID: "
                                    + productId
                                    + " ,OPIS: "
                                    + productDescription
                                    + " ,WYSTAWIAJĄCY: "
                                    + seller);
                }

                System.out.println("\nOSTATNI HARDCODOWANY PRZEDMIOT Z BAZY TO:\n"
                        + productDescription
                        + "\n"
                        + "jego id w bazie to: "
                        + productId
                        + "\n");

                //writing out only new products
                System.out.println("NOWY PRZEDMIOT:");

                //final for thread purposes
                final int finalProductId = productId;

                TimerTask task = new TimerTask() {
                   @Override
                   public void run() {
                       try {
                           getNewProduct(finalProductId);
                       } catch (IOException e) {
                           e.printStackTrace();
                       } catch (ParseException e) {
                           e.printStackTrace();
                       }
                   }
                };

                //Scheduled printing out of new products
                Timer timer = new Timer("Timer");
                timer.scheduleAtFixedRate(task, 1000L, 7000L);

                //if GET request failed
            } else {
                System.out.println("NIE UDAŁO SIĘ WYKONAĆ ZAPYTANIA GET");
            }
        }



        public static HttpURLConnection getConnection(String spec)
                throws IOException {

            URL url = new URL(spec);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            return connection;
        }

        public static StringBuffer readResponse(HttpURLConnection connection,
                                                String readLine)
                throws IOException {

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer response = new StringBuffer();

            while ((readLine = in.readLine()) != null) {
                response.append(readLine);
            }
            in.close();

            return response;
        }

        public static void getNewProduct(int lastOldProductId)
                throws IOException, ParseException {

            HttpURLConnection connection = getConnection("http://localhost:8080/products");
            StringBuffer response = readResponse(connection, "");
            Object obj = new JSONParser().parse(response.toString());
            JSONArray ja = (JSONArray) obj;
            Iterator itr = ja.iterator();

            int newProductId = 0;
            JSONObject jo = null;

            while (itr.hasNext()) {
                jo = (JSONObject) itr.next();
                newProductId = Integer.parseInt(jo.get("id").toString());

                if (newProductId > lastOldProductId){
                    System.out.println("ID: "
                            + jo.get("id").toString()
                            + " ,Nazwa: "
                            + jo.get("name").toString()
                            + " ,WYSTAWIAJĄCY: "
                            + jo.get("seller"));
                }
            }
        }
}